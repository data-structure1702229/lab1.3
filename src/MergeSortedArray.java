public class MergeSortedArray {
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
   
        int k = m + n - 1;

        for (int i = m + n - 1; i >= 0; i--) {
            if (m-1 >= 0 && n-1 >= 0) {
                if (nums1[m-1] > nums2[n-1]) {
                    nums1[i] = nums1[m-1];
                    m-=1;
                } else {
                    nums1[i] = nums2[n-1];
                    n-=1;
                }
            } else if (n-1 >= 0) {
                nums1[i] = nums2[n-1];
                n-=1;
            }
        }
    }
   

    public static void main(String[] args) {
        // Example 1
        int[] nums1 = {1, 2, 3, 0, 0, 0};
        int m = 3;
        int[] nums2 = {2, 5, 6};
        int n = 3;
        System.out.print("nums1 = ");
        printArray(nums1); 
        System.out.println("   m = "+m);
        System.out.print("nums2 = ");
        printArray(nums2); 
        System.out.println("   n = "+n);
        merge(nums1, m, nums2, n);
        System.out.print("Output:  ");
        printArray(nums1); 
        

    }
    static void printArray(int[] arr){
        System.out.print("[");
        for(int i=0; i<arr.length; i++){
            if(i==arr.length-1){
                System.out.print(arr[i]);
            }else{
                System.out.print(arr[i]+",");
            }
        }
        System.out.print("]");
    }

}
